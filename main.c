/*
* Nazev souboru: proj3.c
* Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz
* Datum: 2. 12. 2009
* Projekt: Matice, projekt cislo 3 pro predmet IZP
* Popis programu: Program zpracovava matici nebo vice matic podle zadanych parametru.
*                 Jednotlive matice jsou definovany v souborech. Jmena techto souboru jsou zadana jako parametry
*                 pri spousteni programu.
*                 Program umi scitat 2 matice, nasobit 2 matice, vypocitat vyraz (A+A)*(B+B),
*                 zjistit, zda je zadana matice souvisla a provest tercovou rotaci dane matice.
*/

/** Knihovny potrebne pro beh programu.**/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Funkce, ktera po zavolani vypise napovedu pro tento program.**/
void help(void)
{
    printf("Program  (2009)\n"
          "Autor: Vera Mullerova \n"
          "**************************** \n"
          " Program zpracovava matici nebo vice matic, ktere bere ze souboru. Jmena souboru, z kterych\n"
          " bere program matice, jsou zadany jako parametry pri spousteni programu.\n"
          " Program pracuje s maticemi podle zadanych parametru a vyslednou matici vypise na standardni vystup.\n"
          " \n"
          " Popis parametru: \n"
          "     -h                  zobrazi tuto napovedu \n"
          "     --add x.txt y.txt   secte 2 matice, ktere jsou nadefinovane v souborech x.txt a y.txt\n"
          "     --mult x.txt y.txt  vynasobi 2 matice, ktere jsou nadefinovane v souborech x.txt a y.txt\n"
          "     --expr x.txt y.txt  vypocita vyraz (A+A)*(B+B) 2 matic A a B, ktere jsou nadefinovane v souborech x.txt a y.txt\n"
          "     --cont x.txt        zjisti, zda je matice v souboru x.txt souvisla\n"
          "     --drot x.txt        provede tercovou rotaci matice nadefinovane v souboru x.txt\n"
    );
}

/** Vyctovy typ errors, ve kterem jsou definovane chyby, ktere se mohou v programu objevit. **/
enum errors
{
    EROK, // bez chyby
    ERPARAM, // spatny parametr
    EROPEN, // chyba pri otevirani souboru
    ERCLOSE, // chyba pri zavirani souboru
    ERNUMBER, // chyba pri cteni cisla ze souboru
    ERMEMBER, // chyba pri alokaci pameti
    ERFEW, // maly pocet cisel v souboru
    ERMORE, // v souboru je vice cisel nez jsou rozmery matice
    ERREQ, // nejsou splneny pozadavky pro dany vypocet
    ERUNKNOW // neznama chyba
};

/** Definice pole s chybovymi hlaskami. **/
const char *ERRORS[] =
{
    "Vse v poradku.",
    "Spatne zadane parametry.",
    "Vyskytla se chyba pri otevirani souboru.",
    "Vyskytla se chyba pri zavirani souboru.",
    "Chyba pri cteni ze souboru. Cteny znak neni cislo.",
    "Chyba pri alokaci pameti. Neni dostatek mista.",
    "Matice v souboru ma mensi rozmery, nez udavaji prvni 2 cisla \n"
    "- pocet radku a pocet sloupcu.",
    "V souboru je vice cisel nez udavaji rozmery matice. \n"
    "Pocita se pouze s tolika cisly, jak je velka matice.",
    "U takto zadanych matic nelze dany vypocet provest.",
    "Neznama chyba."
};

/**
* Funkce vypisujici chybove hlasky.
* @param error Chyba z vyctu chyb.
**/
void statementError(int error)
{
    if (error < EROK || error >= ERUNKNOW) //pokud neni chybova hlaska mezi EROK a ERNEVIM, tak se stalo neco necekaneho
        error = ERUNKNOW; //a vypise se mi chybova hlaska ERNEVIM
    fprintf(stderr, "%s \n", ERRORS[error]); //jinak se vypise prislusna chyba, ktera v programu opravdu nastala
}

/**
* Struktura pro zpracování matice ze souboru.
* rows - pocet radku matice
* cols - pocet sloupcu matice
* matrix - ukazatel na jednotliva pole matice
**/
typedef struct matrix
{
    int rows;
    int cols;
    int **matrix;
} TMatrix;

/**
* Funkce, ktera zavre pozadovany soubor a pripadne detekuje chybu pri zavirani souboru.
* @param matrix Soubor, ktery se ma zavrit.
* @param error Chyba.
**/
int closeFile(FILE *matrix)
{
    int error = EROK;
    if (fclose(matrix) == EOF)
        return error = ERCLOSE;
    else
        return error;
}

/**
* Funkce, ktera alokuje tak velkou pamet, jaka je potreba podle rozmeru matice.
* @param rows Pocet radku matice.
* @param cols Pocet sloupcu matice.
* @param error Chyba.
**/
int allocMatrix(TMatrix *M)
{
    int error = EROK;
    TMatrix H = *M;
    H.matrix = malloc(H.rows*sizeof(int *)); // alokace pameti pro pole, ktere bude ukazovat na jednotlive radky
    if (H.matrix == NULL)
    {
        *M = H;
        return error = ERMEMBER;
    }
    for (int i=0; i<H.rows; i++) // alokace pameti pro jednotlive radky
    {
        H.matrix[i] = malloc(H.cols*sizeof(int));
        if (H.matrix[i] == NULL)
        {
            *M = H;
            return error = ERMEMBER;
        }
    }
    *M = H;
    return error;
}

/**
* Funkce, ktera uvolni pamet.
* @param M Struktura matice, u ktere se ma pamet uvolnit.
**/
void deallocMatrix(TMatrix M)
{
    for (int i=0; i<M.rows; i++)
        free(M.matrix[i]);
    free(M.matrix);
}

/**
* Funkce, ktera nacte ze souboru rozmery matice.
* @param M Struktura s matici.
* @param matrix Soubor, ze ktereho se cte.
* @param pass Pocet prectenych cisel ze souboru.
* @param error Chyba.
**/
int sizeMatrix(TMatrix *M, FILE *matrix, int *pass)
{
    int error = EROK;
    TMatrix H = *M;
    int passage = *pass;
    int verif, number = 0;
    while (passage < 2) // ctou se rozmery matice (prvni 2 cisla ze souboru)
    {
        verif = fscanf(matrix, "%d", &number); // cteni ze souboru
        if (verif != 1) // overeni uspesnosti precteni cisla typu int
        {
            closeFile(matrix);
            H.rows = 0;
            H.cols = 0;
            *M = H;
            return error = ERNUMBER;
        }
        if (passage == 0)
            H.rows = number; // definice poctu radku
        else
            H.cols = number; // definice poctu sloupcu
        passage++;
    }
    *pass = passage;
    *M = H;
    return error;
}

/**
* Funkce, ktera naplni matici cisly ze souboru.
* @param M Struktura s matici, ktera se ma naplnit.
* @param matrix Soubor, ze ktereho se cte.
* @param error Chyba.
**/
int fillMatrix(TMatrix *M, int *pass, FILE *matrix)
{
    int error = EROK;
    TMatrix H = *M;
    int passage = *pass;
    int verif, number = 0;
    for(int i=0; i<H.rows; i++) //radky
    {
        for(int j=0; j<H.cols; j++) //sloupce
        {
            if ((verif = fscanf(matrix, "%d", &number)) == EOF)
            {
                closeFile(matrix);
                *M = H;
                return error = ERFEW;
            }
            if(verif != 1)
            {
                closeFile(matrix);
                *M = H;
                return error = ERNUMBER;
            }
            H.matrix[i][j] = number; // nacteni prislusneho policka v matici ze souboru
            passage++;
        }
    }
    *pass = passage;
    *M = H;
    return error;
}

/**
* Funkce, ktera se postara o precteni matice ze souboru.
* Pomoci volani dalsich funkci pripravi matici pro dalsi praci s ni.
* Zjisti rozmery matice ze souboru, alokuje pro ni potrebnou pamet,
* nacte do ni data ze souboru a pripadne detekuje chyby.
* @param name Jmeno souboru, ze ktereho se ma nacist matice.
* @param error Chyba.
**/
int readFile(char *name, TMatrix *M)
{
    TMatrix H = *M;
    int error = EROK;
    FILE *matrix;
    matrix = fopen(name, "r"); // otevreni souboru pro cteni
    if (matrix == NULL) // detekce chyby pri otevirani souboru
    {
        *M = H;
        return error = EROPEN;
    }
    int number = 0, pass = 0; // deklarace pomocnych promennych
    error = sizeMatrix(&H, &*matrix, &pass);
    if (error != EROK) // overeni spravnosti alokace pameti
    {
        *M = H;
        return error;
    }
    int count = H.rows * H.cols; // zjisteni poctu cisel v matici
    error = allocMatrix(&H); // alokace potrebne pameti
    if (error != EROK) // overeni spravnosti alokace pameti
    {
        *M = H;
        return error;
    }
    error = fillMatrix(&H, &pass, &*matrix);
    if (error != EROK) // overeni spravnosti naplneni matice
    {
        *M = H;
        return error;
    }
    while (fscanf(matrix,"%d", &number) != EOF)
    { // kontroluji, zda v souboru neni vice cisel, nez jak udavaji parametry matice
        pass++;
    }
    if ((pass-2)> count) // kontrola mnozstvi dat v souboru (zda neni v souboru vice hodnot nez je treba)
        statementError(ERMORE); // varovani, ze je v souboru vice cisel, nez je velikost matice
    closeFile(matrix); // zavreni souboru
    if (error != EROK) // kontrola spravnosti zavreni souboru
    {
        *M = H;
        return error;
    }
    *M = H;
    return error;
}

/**
* Funkce, ktera secte 2 zadane matice.
* @param A Struktura s matici.
* @param B Struktura s matici.
* @param error Chyba.
**/
int addMatrix (TMatrix A, TMatrix B, TMatrix *C)
{
    TMatrix H = *C;
    int error = EROK;
    if (A.rows != B.rows || A.cols != B.cols)
    {
        *C = H;
        return error = ERREQ;
    }
    H.rows = A.rows; // nastaveni rozmeru vysledne matice
    H.cols = A.cols;
    error = allocMatrix(&H); // alokace pameti pro vyslednou matici
    if (error != EROK) // overeni spravnosti alokace pameti
    {
        *C = H;
        return error;
    }
    for(int r=0; r<H.rows; r++)
    {
        for(int s=0; s<H.cols; s++)
            H.matrix[r][s] = A.matrix[r][s] + B.matrix[r][s];
    }
    *C = H;
    return error;
}

/**
* Funkce, ktera vynasobi 2 matice.
* @param A Struktura s matici.
* @param B Struktura s matici.
* @param error Chyba.
**/
int multMatrix(TMatrix A, TMatrix B, TMatrix *C)
{
    TMatrix H = *C;
    int error = EROK;
    if (A.cols != B.rows)
    {
        *C = H;
        return error = ERREQ;
    }
    H.rows = A.rows; // nastaveni rozmeru vysledne matice
    H.cols = B.cols;
    error = allocMatrix(&H); // alokace pameti pro vyslednou matici
    if (error != EROK) // overeni spravnosti alokace pameti
    {
        *C = H;
        return error;
    }
    // nastaveni vsech prvku vysledne matice na hodnotu 0
    for(int r=0; r<H.rows; r++)
    {
        for(int s=0; s<H.cols; s++)
            H.matrix[r][s] = 0;
    }
    // nasobeni 2 matic a zapsani vysledku do matice vysledne
    for(int r=0; r<H.rows; r++)
    {
        for(int s=0; s<H.cols; s++)
        {
            for(int j=0; j<A.cols; j++)
                H.matrix[r][s] = H.matrix[r][s] + A.matrix[r][j]*B.matrix[j][s];
        }
    }
    *C = H;
    return error;
}

/**
* Funkce, ktera zjisti zda je zadana matice matice souvisla.
* @param A Struktura s matici.
**/
int contMatrix(TMatrix A)
{
    int i=0, j=0, k=0, l=0;
    for (i=0;i<A.rows;i++) //prochazim radky matice
    {
        for (j=0;j<(A.cols-1);j++)
        {   //porovnavam cislo s cislem napravo od nej
            if (A.matrix[i][j]!=A.matrix[i][j+1] && A.matrix[i][j]!=(A.matrix[i][j+1]+1) && A.matrix[i][j]!=(A.matrix[i][j+1]-1))
                return 1; //2 cisla vedle sebe nejsou stejna nebo odlisna o 1
            //porovnavam cislo s cislem vpravo dole (po diagonale)
            if (i<(A.rows-1))
            {
                if (A.matrix[i][j]!=A.matrix[i+1][j+1] && A.matrix[i][j]!=(A.matrix[i+1][j+1]+1) && A.matrix[i][j]!=(A.matrix[i+1][j+1]-1))
                    return 1; //cisla nejsou stejna nebo odlisna o 1
            }
        }
    }
    for (k=0;k<A.cols;k++) //prochazim sloupce matice
    {
        for (l=0;l<(A.rows-1);l++)
        {   //porovnama cisla pod sebou
            if (A.matrix[l][k]!=A.matrix[l+1][k] && A.matrix[l][k]!=(A.matrix[l+1][k]+1) && A.matrix[l][k]!=(A.matrix[l+1][k]-1))
                return 1; //2 cisla pod sebou nejsou stejna nebo odlisna o 1
            if (l>0 && k<(A.cols-1))
            {   //porovnavam cisla vpravo nahoru (po diagonale)
                if (A.matrix[l][k]!=A.matrix[l-1][k+1] && A.matrix[l][k]!=(A.matrix[l-1][k+1]+1) && A.matrix[l][k]!=(A.matrix[l-1][k+1]-1))
                    return 1; //cisla nejsou stejna nebo odlisna o 1
            }
        }
    }
    return 0;
}

/**
* Funkce, ktera spocita, kolik "okruhu" matice ma.
* @param M Struktura s matici.
**/
int calPass(TMatrix M)
{
    int pass = 0;
    int i=0, j=0;
    j = M.cols-1;
    while (i<M.rows && j>=0)
    { //prochazim po diagonale od praveho horniho rohu dolu a pocitam kolik je zde cisel
        i++;
        j--;
        pass++;
    }
    pass = (pass/2) + (pass % 2); //z poctu cisel na diagonale vypocitam kolik je okruhu
    return pass;
}

/**
* Funkce, ktera zapise cisla z "okruhu" do pole.
* @param field Pole, do ktereho nacitam hodnoty.
* @param M Struktura s matici.
* @param pass Cislo, ktere udava, s kolikatym okruhem se pracuje.
**/
void readField(int field[], TMatrix M, int pass)
{
    int i=0, j=0, k=0, l=0;
    int e=0, c=(M.cols-1), r=(M.rows-1);
    if (M.rows - 2*pass == 1) //pokud uz nejde o kruh, ale pouze o radek
    {
        for (i=0;i<(M.cols-2*pass);i++) //zacina se v pravem hodnim rohu
        {   //zapise cisla vnitrniho radku
            field[e] = M.matrix[0+pass][c-pass];
            e++;
            c--;
        }
    }
    else if (M.cols - 2*pass == 1) //pokud uz nejde o kruh, ale pouze o sloupec
    {
        for (j=0;j<(M.rows-2*pass);j++)
        {   //zapise cisla vnitrniho sloupce
            field[e] = M.matrix[j+pass][0+pass];
            e++;
        }
    }
    else
    {
        for (i=0;i<(M.cols-2*pass);i++) //zacina se v pravem hodnim rohu
        {   //zapise cisla horni radky do pole
            field[e] = M.matrix[0+pass][c-pass];
            e++;
            c--;
        }
        for (j=1;j<(M.rows-2*pass);j++)
        {   //zapise cisla leve radky do pole bez jiz napsaneho nejhornejsiho cisla
            field[e] = M.matrix[j+pass][0+pass];
            e++;
        }
        for (k=1;k<(M.cols-2*pass);k++)
        {   //zapise cisla dolni radky do pole bez nejlevejsiho
            field[e] = M.matrix[M.rows-1-pass][k+pass];
            e++;
        }
        for (l=0;l<(M.rows-2-2*pass);l++)
        {   //zapise cisla leve radky do pole bez nejspodnejsiho
            field[e] = M.matrix[r-1-pass][M.cols-1-pass];
            e++;
            r--;
        }
    }
}

/**
* Opak funkce readField. Funkce zapisuje prvky z pole zpet do matice.
* @param field Pole, do ktereho nacitam hodnoty.
* @param M Struktura s matici.
* @param pass Cislo, ktere udava, s kolikatym okruhem se pracuje.
**/
TMatrix writeField(int field[], TMatrix M, int pass)
{
    int i=0, j=0, k=0, l=0;
    int e=0, c=(M.cols-1), r=(M.rows-1);

    if (M.rows - 2*pass == 1) //pokud uz nejde o kruh, ale pouze o radek
    {
        for (i=0;i<(M.cols-2*pass);i++)
        {   //zapise cisla vnitrni radky do pole
            M.matrix[0+pass][c-pass] = field[e];
            e++;
            c--;
        }
    }
    else if (M.cols - 2*pass == 1) //pokud uz nejde o kruh, ale pouze o sloupec
    {
        for (j=0;j<(M.rows-2*pass);j++)
        {   //zapise cisla vnitriho sloupce do
            M.matrix[j+pass][0+pass] = field[e];
            e++;
        }
    }
    else
    {
        for (i=0;i<(M.cols-2*pass);i++) //zacina se v pravem hodnim rohu
        {   //zapise cisla horni radky do pole
            M.matrix[0+pass][c-pass] = field[e];
            e++;
            c--;
        }
        for (j=1;j<(M.rows-2*pass);j++)
        {   //zapise cisla leve radky do pole bez jiz napsaneho nejhornejsiho cisla
            M.matrix[j+pass][0+pass] = field[e];
            e++;
        }
        for (k=1;k<(M.cols-2*pass);k++) //zapise cisla dolni radky do pole bez nejlevejsiho
        {
            M.matrix[M.rows-1-pass][k+pass] = field[e];
            e++;
        }
        for (l=0;l<(M.rows-2-2*pass);l++)
        {   //zapise cisla leve radky do pole bez nejspodnejsiho
            M.matrix[r-1-pass][M.cols-1-pass] = field[e];
            e++;
            r--;
        }
    }
    return M;
}

/**
* Funkce, ktera pomoci dalsich funkci provede tercovou rotaci zadane matice.
* @param M Struktura s matici.
**/
TMatrix drotMatrix(TMatrix M)
{
    int i=0, j=0, k=0;
    int pass = calPass(M);
    int count = 0, aux = 0;
    int e=0;
    for(i=0;i<pass;i++) // postupne prochazim vsechny okruhy matice a posunuji jejich prvky
    {
        if (M.cols-2*i == 1)
            e = M.rows - 2*i;
        else if (M.rows-2*i == 1)
            e = M.cols - 2*i;
        else
            e = (2*M.rows - 4*i) + (2*M.cols - 4*i) - 4;
        if (e == 0)
            break;
        int *field = malloc(e*sizeof(int));
        readField(field, M, i);
        count = field[0];
        if (count > 0) // bude se posunovat doleva
        {
            for(j=0;j<count;j++) //posouvaji se prvky v poli
            {
                aux = field[e-1];
                for(k=(e-1);k>0;k--)
                    field[k] = field[k-1];
                field[0] = aux;
            }
            writeField(field, M, i);
        }
        else if (count < 0) // bude se posunovat doprave
        {
            for(j=0;j<abs(count);j++) //posouvaji se prvky v poli
            {
                aux = field[0];
                for(k=0;k<(e-1);k++)
                    field[k] = field[k+1];
                field[e-1] = aux;
            }
            writeField(field, M, i);
        }
        else //nebude se posunovat nikam
        {}
        free(field); //uvolneni pameti alokovane pro pole
    }
    return M;
}

/**
* Funkce, ktera vypise pozadovanou matici a jeji rozmery na standardni vystup.
* @param M Struktura s matici.
**/
void writeMatrix(TMatrix M)
{
    printf("%d %d \n", M.rows, M.cols);
    for (int i=0; i<M.rows; i++)
    {
        for(int j=0; j<M.cols; j++)
        {
            printf("%d ", M.matrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

/**
* Funkce, ktera nam vrati cislo podle toho, ktery parametr uzivatel zadal.
* 1 - parametr -h
* 2 - parametr --add
* 3 - parametr --mult
* 4 - parametr --expr
* 5 - parametr --cont
* 6 - parametr --drot
* 7 - chybny 1. parametr
* @param argc Pocet argumentu.
* @param argv Pole textovych retezcu s argumenty.
*/
int findParam(int argc, char *argv[])
{
    if (argc == 2 && strcmp("-h", argv[1]) == 0) // napoveda
        return 1;
    else if (argc == 4 && strcmp("--add", argv[1]) == 0) // scitani matic
        return 2;
    else if (argc == 4 && strcmp("--mult", argv[1]) == 0) // nasobeni matic
        return 3;
    else if (argc == 4 && strcmp("--expr", argv[1]) == 0) // vyraz s maticemi
        return 4;
    else if (argc == 3 && strcmp("--cont", argv[1]) == 0) // souvisla matice
        return 5;
    else if (argc == 3 && strcmp("--drot", argv[1]) == 0) // tercova rotace
        return 6;
    else return 7; // chybny parametr
}

/**
* Hlavni funkce.
* @param argc Pocet argumentu.
* @param argv Pole textovych retezcu s argumenty.
**/
int main(int argc, char *argv[])
{
    int error = EROK;
    int param = findParam(argc, argv);
    TMatrix C = {.rows = 0, .cols = 0, .matrix = NULL};
    TMatrix A = {.rows = 0, .cols = 0, .matrix = NULL};
    TMatrix B = {.rows = 0, .cols = 0, .matrix = NULL};
    TMatrix AA = {.rows = 0, .cols = 0, .matrix = NULL};
    TMatrix BB = {.rows = 0, .cols = 0, .matrix = NULL};
    int result = 3;

    switch (param)
    {
        case 1: help(); break;
        case 2: error = readFile(argv[2], &A);
                if (error != EROK)
                    break;
                error = readFile(argv[3], &B);
                if (error != EROK)
                    break;
                error = addMatrix(A, B, &C);
                break;
        case 3: error = readFile(argv[2], &A);
                if (error != EROK)
                    break;
                error = readFile(argv[3], &B);
                if (error != EROK)
                    break;
                error = multMatrix(A, B, &C);
                break;
        case 4: error = readFile(argv[2], &A);
                if (error != EROK)
                    break;
                error = readFile(argv[3], &B);
                if (error != EROK)
                    break;
                error = addMatrix(A, A, &AA);
                if (error != EROK)
                    break;
                error = addMatrix(B, B, &BB);
                if (error != EROK)
                    break;
                error = multMatrix(AA, BB, &C);
                break;
        case 5: error = readFile(argv[2], &A);
                if (error == EROK)
                    result = contMatrix(A);
                break;
        case 6: error = readFile(argv[2], &A);
                if (error == EROK)
                    C = drotMatrix(A);
                break;
        case 7: error = ERPARAM; break;
        default: error = ERUNKNOW; break;
    }

    if (C.matrix != NULL)
    {
        writeMatrix(C); // na standardni vystup se vypise vysledna matice
        if (param != 6)
        {
            deallocMatrix(C);
        }
    }
    else if (result != 3)
    {
        switch(result) // vypsani vysledku souvisle matice
        {
            case 0: printf("true"); break;
            case 1: printf("false"); break;
            default: error = ERUNKNOW; break;
        }
    }
    else
    {}

    if (error != EROK) //kontrola chybove hlasky a pripadne jeji vypsani, pokud se behem programu nejaka chyba vyskytla
    { // pred ukoncenim celeho programu je potreba dealokovat vsechnu naalokovanou pamet
        switch(param)
        {
            case 1: break;
            case 2:
            case 3:
            case 4: deallocMatrix(A);
                    deallocMatrix(B);
                    deallocMatrix(AA);
                    deallocMatrix(BB);
                    break;
            case 5: deallocMatrix(A); break;
            case 6: deallocMatrix(A); break;
            default: deallocMatrix(A);
                    deallocMatrix(B);
                    deallocMatrix(AA);
                    deallocMatrix(BB);
                    break;
        }
        statementError(error); // vypis chyboveho hlaseni
        return EXIT_FAILURE;
    }
    else
    { // dealokace veskere naalokovane pameti pred ukoncenim programu
        switch(param)
        {
            case 1: break;
            case 2:
            case 3:
            case 4: deallocMatrix(A);
                    deallocMatrix(B);
                    deallocMatrix(AA);
                    deallocMatrix(BB);
                    break;
            case 5: deallocMatrix(A); break;
            case 6: deallocMatrix(A); break;
            default: deallocMatrix(A);
                    deallocMatrix(B);
                    deallocMatrix(AA);
                    deallocMatrix(BB);
                    break;
        }
        return EXIT_SUCCESS;
    }
}
