# README #

Third programming task for the course Introduction to Programming Systems: 

Program process matrix or more matrices based on input parameters.
Matrices are defined in file which names are specified as input parameters.
Program can add 2 matrices, multiply 2 matrices, count an expression (A+A)*(B+B), check if the matrix is continuous and make a target rotation of the matrix.


Compile project by command

```
#!c

make
```